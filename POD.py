#!/usr/bin/env python3
'''
    This script performs a POD on velocity field FEM data of a cylinder wake flow
    at Re=100 (100 Mo).
    The code is sequential and aims more to be readable than efficient.
    It reads data in "data/", and write results in "resutls/".

    "##" comments corresponds to corrected lines.

    To run the script: ./POD.py
'''

####################################
# Import
####################################
import numpy as np
from scipy import sparse 
from scipy.io import hb_read # To read Harwell-Boeing format
import pickle # To easily save/read

import logging # For fancy prints
logger = logging.getLogger('Main') # For fancy prints
logging.basicConfig(level = logging.INFO) # For fancy prints

####################################
# Parameters
####################################
logger.info('Parameters')

N_modes_kept = 50 # Maximal number of kept modes

dir_data = 'data/' # Directory to read/write data
dir_out = 'results/' # Directory to read/write data

file_U = 'U_simu.pickle' # File containing simulation data
file_mass = 'Mu.hb'      # File containing the FEM mass matrix
file_POD = 'POD.pickle'  # File to write the POD results

####################################
# Load data
####################################
logger.info('Load data')

# Load U
(t, U_tot) = pickle.load(open(dir_data + file_U, 'rb'))
logger.info('U loaded. Size: {}'.format(U_tot.shape))

# Sizes
(N, Nt) = U_tot.shape

# Mass matrix. Assumed size: (N, N) but is sparse
Mu = hb_read(dir_data + file_mass)

####################################
# Prepare data
####################################
logger.info('Prepare data')

# Time-average. Size: (N,)
Um = np.mean(U_tot, axis=1) ##

# Fluctuation. Size: (N, Nt)
u = U_tot - np.tile(Um[:,np.newaxis], (1, Nt)) ##

####################################
# Define innerproduct
####################################
logger.info('Define inner product')
#-------------------------------------------
# (f,g) = \int f(x) g(x) dx = f^T @ M @ g
# with M the mass matrix
#
# input: f, g real vectors or matrices with space vectors sorted in columns
#        M: real mass matrix (dense or sparse)
#
# output: innerproduct, or Gramm matrix
#
def innerproduct_space(f, g, M): # real space vectors are in columns
    return f.T @ M @ g ##

#-------------------------------------------
# (a,b) = 1/T int a(t) b(t) dt = a @ b^T / Nt
# with Nt the number of time samples
#
# input: a, b real arrays or matrices with time series sorted in rows
#        Nt: number of samples
#
# output: innerproduct, or Gramm matrix
#
def innerproduct_time(a, b, Nt):
    return a @ b.T / Nt ##

####################################
# Compute correlation matrix
####################################
logger.info('Build correlation matrix')
C = innerproduct_space(u, u, Mu) / Nt ##

logger.info('Correlation matrix assembled. Size: {}'.format(C.shape))

####################################
# Compute eigenvalue problem
####################################
logger.info('Solve eigenvalue problem')
(lambda_POD, a_POD) = np.linalg.eigh(C) ##

####################################
# Sort in descending order
####################################
logger.info('Sort modes')
lambda_POD[:] = lambda_POD[::-1]
a_POD[:,:] = a_POD[:,::-1]

# Remove spurious modes and set the vector sizes.
logger.info('Remove modes with small eigenvalue')
big_enough = lambda_POD[np.where(lambda_POD[:] > 1e-9*lambda_POD[0])]
N_POD = big_enough.size
N_modes_kept = np.minimum(N_modes_kept, N_POD)
lambda_POD = lambda_POD[range(N_POD)] # Keep all non-spurious eigenvalues
a_POD = a_POD[:,range(N_modes_kept)]  # Keep only relevant modes
logger.info("{} non-zero modes, {} modes are kept".format(N_POD, N_modes_kept))

####################################
# Normalise a_POD
####################################
logger.info('Normalise a_POD')
for imode in range(N_modes_kept):
    a_POD[:,imode] = a_POD[:,imode]*np.sqrt(Nt*lambda_POD[imode]) ##

####################################
# Compute spatial modes
####################################
logger.info('Compute spatial modes')
Phi_POD = innerproduct_time(u, a_POD.T, Nt) ##

logger.info('Normalise spatial modes')
for imode in range(N_modes_kept): # not the most efficient but readable
    Phi_POD[:,imode] = Phi_POD[:,imode] / lambda_POD[imode] ##

####################################
# Check normalisation
####################################
logger.info('Check normalisation')
G = innerproduct_space(Phi_POD, Phi_POD, Mu) ##
I = np.identity(N_modes_kept, dtype = np.float64)
logger.debug(G)
logger.info('||G - I|| / ||I|| = {:.2f} %'.format( np.linalg.norm(G - I) / np.linalg.norm(I) * 100.))

####################################
# Check reconstruction
####################################
logger.info('Check reconstruction')
U_reconstruction = np.tile(Um[:,np.newaxis], (1, Nt)) + Phi_POD @ a_POD.T ##
logger.info('||U_tot - U_reconstruction|| / ||U_tot|| = {:.2f} %'.format( np.linalg.norm(U_tot - U_reconstruction) / np.linalg.norm(U_tot) * 100.))

####################################
# Check a_POD
####################################
H = innerproduct_time(a_POD.T, a_POD.T, Nt) @ (1./lambda_POD[0:N_modes_kept])
I1 = np.ones((N_modes_kept,), dtype=np.float64)
logger.info('||H - 1|| / ||1|| = {:.2f} %'.format( np.linalg.norm(H - I1) / np.linalg.norm(I1) * 100.))

####################################
# Save POD
####################################
pickle.dump((lambda_POD, Phi_POD, a_POD), open(dir_out + file_POD, 'wb'))

