Written by Gilles Tissot, 2024/03/26.

This repository provides a simple code performing proper orthogonal decomposition (POD) on velocity field FEM data of a cylinder wake flow at Re=100 (100 Mo).
The code is sequential and aims more to be readable than efficient.
It reads data in "data/", and write results in "resutls/".


The code is written in Python 3. A notebook and a script version are provided.
A visualisation notebook visu_POD.ipnb is also provided to display the results.

    Create results directory: mkdir results
    To run the script: ./POD.py
